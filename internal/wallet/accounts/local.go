package accounts

import (
	"encoding/json"
	"io"
	"log"
	"os"
	"path"
)

func LoadWallets(wallets *Accounts, fileName string) {
	filePath := path.Join(walletsPath, fileName)
	file, err := os.Open(filePath)
	if err != nil && !os.IsNotExist(err) {
		log.Fatal("[LoadWallets] Err open file", err)
	}
	if os.IsNotExist(err) {
		os.MkdirAll(walletsPath, 0777)
		return
	}
	if err := json.NewDecoder(file).Decode(&wallets); err != nil && err != io.EOF {
		log.Fatal("Err when try to load wallets file:", err)
	}
}

func SaveWallets(wallets *Accounts, fileName string) {
	file, err := os.Create(path.Join(walletsPath, fileName))
	if err != nil {
		log.Fatal(err)
	}
	if err := json.NewEncoder(file).Encode(wallets); err != nil {
		log.Fatal("Err when try to save wallets file:", err)
	}
}
