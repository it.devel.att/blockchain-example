package accounts

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"log"
	"math/big"
)

const walletsPath = "data/wallets"
const WalletsFile = "wallets.json"

var curve = elliptic.P224()

type Accounts map[string]Account

func (a Accounts) SaveAccount(account Account) {
	a[account.Address] = account
}

func (a Accounts) PrintAddresses() {
	for key, _ := range a {
		fmt.Println("address: ", key)
	}
}

type Account struct {
	Address       string
	PrivateHexKey string
	PublicHexKey  string
}

func GenerateAccount() Account {
	privateKey, publicKey := generatePair()
	account := Account{
		PrivateHexKey: MarshallPrivateKeyHEX(privateKey),
		PublicHexKey:  MarshallPublicKeyHEX(publicKey),
	}
	account.Address = GenerateShortAddress(account.PublicKey())
	return account
}

func GenerateShortAddress(key *ecdsa.PublicKey) string {
	addressHash := sha1.Sum(elliptic.Marshal(key.Curve, key.X, key.Y))
	return hex.EncodeToString(addressHash[:])
}

func generatePair() (*ecdsa.PrivateKey, *ecdsa.PublicKey) {
	privKey, err := ecdsa.GenerateKey(curve, rand.Reader)
	if err != nil {
		log.Fatal(err)
	}
	return privKey, &privKey.PublicKey
}

func MarshallPublicKeyHEX(key *ecdsa.PublicKey) string {
	data := elliptic.Marshal(key.Curve, key.X, key.Y)
	return hex.EncodeToString(data)
}

func UnmarshallHEXPublicKey(key string) *ecdsa.PublicKey {
	data, err := hex.DecodeString(key)
	if err != nil {
		panic(err)
	}
	var publicKey ecdsa.PublicKey
	publicKey.Curve = curve
	publicKey.X, publicKey.Y = elliptic.Unmarshal(curve, data)
	return &publicKey
}

func MarshallPrivateKeyHEX(key *ecdsa.PrivateKey) string {
	return hex.EncodeToString(key.D.Bytes())
}

func UnmarshallHEXPrivateKey(privKeyHex string) *ecdsa.PrivateKey {
	privDbytes, _ := hex.DecodeString(privKeyHex)

	pk := new(ecdsa.PrivateKey)
	pk.D = new(big.Int).SetBytes(privDbytes)
	pk.PublicKey.Curve = curve
	pk.PublicKey.X, pk.PublicKey.Y = pk.PublicKey.Curve.ScalarBaseMult(pk.D.Bytes())
	return &ecdsa.PrivateKey{
		PublicKey: pk.PublicKey,
		D:         pk.D,
	}
}

func ComparePrivateWithPublic(privateKeyHex, publicKeyHex string) bool {
	privKey := UnmarshallHEXPrivateKey(privateKeyHex)
	pubKey := UnmarshallHEXPublicKey(publicKeyHex)
	return privKey.PublicKey.Equal(pubKey)
}

func (a *Account) PublicKeyString() string {
	return hex.EncodeToString(a.PublicKeyBytes())
}

func (a *Account) PublicKeyBytes() []byte {
	pubKey := a.PrivateKey().PublicKey
	return elliptic.Marshal(pubKey.Curve, pubKey.X, pubKey.Y)
}

func (a *Account) PrivateKey() *ecdsa.PrivateKey {
	return UnmarshallHEXPrivateKey(a.PrivateHexKey)
}

func (a *Account) PublicKey() *ecdsa.PublicKey {
	return &UnmarshallHEXPrivateKey(a.PrivateHexKey).PublicKey
}

func (a *Account) Sign(data []byte) string {
	result, err := a.PrivateKey().Sign(rand.Reader, data, nil)
	if err != nil {
		log.Fatal(err)
	}
	return hex.EncodeToString(result)
}
