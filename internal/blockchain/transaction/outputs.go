package transaction

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"strconv"
)

type Output struct {
	Amount  uint64 `json:"amount"`
	Address string `json:"address"`
}

func (i *Output) Hash() string {
	data := fmt.Sprintf(i.Address + strconv.Itoa(int(i.Amount)))
	hash := sha256.Sum256([]byte(data))
	return hex.EncodeToString(hash[:])
}

type Outputs []Output

func (outputs Outputs) Hash() string {
	hashes := ""
	for i := range outputs {
		hashes += outputs[i].Hash()
	}
	hash := sha256.Sum256([]byte(hashes))
	return hex.EncodeToString(hash[:])
}
