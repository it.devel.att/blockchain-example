package transaction

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"strconv"
)

type Input struct {
	Amount  uint64          `json:"amount"`
	Address string          `json:"address"`
	Output  *PreviousOutput `json:"output,omitempty"`
}

type PreviousOutput struct {
	BlockNumber     int
	BlockHash       string
	TransactionHash string
}

func (o *PreviousOutput) Hash() string {
	if o == nil {
		return ""
	}
	data := strconv.Itoa(o.BlockNumber) + o.BlockHash + o.TransactionHash
	hash := sha256.Sum256([]byte(data))
	return hex.EncodeToString(hash[:])
}

func (i *Input) Hash() string {
	data := fmt.Sprintf(i.Address + strconv.Itoa(int(i.Amount)) + i.Output.Hash())
	hash := sha256.Sum256([]byte(data))
	return hex.EncodeToString(hash[:])
}

type Inputs []Input

func (inputs Inputs) Hash() string {
	hashes := ""
	for i := range inputs {
		hashes += inputs[i].Hash()
	}
	hash := sha256.Sum256([]byte(hashes))
	return hex.EncodeToString(hash[:])
}
