package transaction

import (
	"crypto/sha256"
	"encoding/hex"
	"strconv"
	"time"
)

type Transaction struct {
	Hash       string  `json:"hash"`
	IsCoinBase bool    `json:"is_coin_base"`
	Time       int64   `json:"time"`
	Inputs     Inputs  `json:"inputs"`
	Outputs    Outputs `json:"outputs"`
}

func NewCoinBase(address string) *Transaction {
	tx := &Transaction{
		IsCoinBase: true,
		Inputs:     Inputs{},
		Outputs: Outputs{
			{
				Amount:  50, // TODO Other way
				Address: address,
			},
		},
	}
	tx.Seal(time.Now())
	return tx
}

func NewTransaction() *Transaction {
	tx := &Transaction{
		IsCoinBase: false,
		Inputs:     Inputs{},
		Outputs:    Outputs{},
	}
	return tx
}

func (t *Transaction) OutputSum() uint64 {
	var coins uint64
	for i := range t.Outputs {
		coins += t.Outputs[i].Amount
	}
	return coins
}

func (t *Transaction) Seal(now time.Time) {
	t.Time = now.UnixNano()
	t.Hash = t.GenHash()
}

func (t *Transaction) GenHash() string {
	isCoinBase := "0"
	if t.IsCoinBase {
		isCoinBase = "1"
	}
	data := t.Inputs.Hash() + t.Outputs.Hash() + strconv.Itoa(int(t.Time)) + isCoinBase
	hash := sha256.Sum256([]byte(data))
	return hex.EncodeToString(hash[:])
}

func (t *Transaction) IsValid() bool {
	return t.hashValid()
}

func (t *Transaction) hashValid() bool {
	return t.Hash == t.GenHash()
}
