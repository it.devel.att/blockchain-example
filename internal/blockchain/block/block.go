package block

import (
	"strconv"
	"time"

	"blockchain-example/internal/blockchain/merkle_tree"
	"blockchain-example/internal/blockchain/proof_of_work"
	"blockchain-example/internal/blockchain/transaction"
)

type Block struct {
	Number       int                        `json:"number"`
	PrevHash     string                     `json:"prev_hash"`
	Hash         string                     `json:"hash"`
	Time         int64                      `json:"time"`
	Nonce        uint64                     `json:"nonce"`
	Complexity   int                        `json:"complexity"`
	MerkleRoot   string                     `json:"merkle_root"`
	MerkleTree   [][]string                 `json:"merkle_tree"`
	Transactions []*transaction.Transaction `json:"transactions"`
}

func GenesisBlock(minerAddress string, now time.Time) *Block {
	block := &Block{
		Number:       1,
		PrevHash:     "",
		Complexity:   proof_of_work.RequiredComplexity(1),
		Time:         now.UnixNano(),
		Transactions: []*transaction.Transaction{transaction.NewCoinBase(minerAddress)},
	}
	return block
}

func NewBlock(minerAddress string, prevBlock *Block, now time.Time) *Block {
	block := &Block{
		Number:       prevBlock.Number + 1,
		PrevHash:     prevBlock.Hash,
		Time:         now.UnixNano(),
		Transactions: []*transaction.Transaction{transaction.NewCoinBase(minerAddress)},
	}
	block.Complexity = proof_of_work.RequiredComplexity(block.Number)
	return block
}

func (b *Block) AddTransaction(tx *transaction.Transaction) bool {
	if tx.IsValid() {
		b.Transactions = append(b.Transactions, tx)
		b.SetMerkleRoot()
		return true
	}
	return false
}

func (b *Block) SetMerkleRoot() {
	b.MerkleRoot, b.MerkleTree = b.CalculateMerkleRootAndTree()
}

func (b *Block) CalculateMerkleRootAndTree() (string, [][]string) {
	hashes := make([]string, len(b.Transactions))
	for i := range b.Transactions {
		hashes[i] = b.Transactions[i].Hash
	}
	return merkle_tree.GenerateRootHash(hashes)
}

func (b *Block) Mining() {
	b.SetMerkleRoot()

	newHash := ""
	for !proof_of_work.IsSatisfyComplexity(b.Number, newHash) {
		b.Nonce++
		newHash = proof_of_work.GenerateHash(b.BaseHash(), b.Nonce)
	}
	b.Hash = newHash
}

func (b *Block) BaseHash() string {
	return strconv.Itoa(b.Number) + b.PrevHash + b.MerkleRoot + strconv.Itoa(b.Complexity) + strconv.Itoa(int(b.Time))
}

func (b *Block) IsValid() bool {
	if b.Hash != proof_of_work.GenerateHash(b.BaseHash(), b.Nonce) {
		return false
	}
	merkleRoot, _ := b.CalculateMerkleRootAndTree()
	if b.MerkleRoot != merkleRoot {
		return false
	}
	return true
}
