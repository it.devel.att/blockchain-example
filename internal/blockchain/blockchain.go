package blockchain

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path"
	"regexp"
	"sort"
	"strconv"
	"sync"
	"time"

	"blockchain-example/internal/blockchain/block"
	"blockchain-example/internal/blockchain/transaction"
)

type Blockchain struct {
	ctx context.Context
	wg  sync.WaitGroup

	utxo             UTXO
	transactionsChan chan *transaction.Transaction
	blocks           []*block.Block
	miningBlock      *block.Block
}

func LoadBlockchain(ctx context.Context) *Blockchain {
	bc := &Blockchain{
		ctx:              ctx,
		transactionsChan: make(chan *transaction.Transaction, 1000),
		blocks:           make([]*block.Block, 0),
		utxo:             UTXO{},
	}
	StateFromFiles(bc)
	return bc
}

const blockChainDir = "data/blocks"

func (bc *Blockchain) NextBlock(minerAddress string) *block.Block {
	blockChainLength := len(bc.blocks)
	if len(bc.blocks) >= 1 {
		previousBlock := bc.BlockAt(blockChainLength - 1)
		return block.NewBlock(minerAddress, previousBlock, time.Now())
	}
	return block.GenesisBlock(minerAddress, time.Now())
}

func (bc *Blockchain) BlockAt(i int) *block.Block {
	return bc.blocks[i]
}

func (bc *Blockchain) AddBlock(b *block.Block) bool {
	if !b.IsValid() {
		log.Printf("[Blockchain] Invalid block for add to chain!")
		return false
	}
	bc.blocks = append(bc.blocks, b)
	for i := range b.Transactions {
		for o := range b.Transactions[i].Outputs {
			output := b.Transactions[i].Outputs[o]
			utxoKey := UTXOKey{
				BlockNumber:     b.Number,
				BlockHash:       b.Hash,
				TransactionHash: b.Transactions[i].Hash,
			}
			if _, ok := bc.utxo[output.Address]; !ok {
				bc.utxo[output.Address] = map[UTXOKey][]UTXOData{utxoKey: {}}
			}
			bc.utxo[output.Address][utxoKey] = append(bc.utxo[output.Address][utxoKey], UTXOData{Amount: output.Amount})
		}
		for k := range b.Transactions[i].Inputs {
			input := b.Transactions[i].Inputs[k]
			if input.Output == nil {
				continue
			}
			utxoKey := UTXOKey{
				BlockNumber:     input.Output.BlockNumber,
				BlockHash:       input.Output.BlockHash,
				TransactionHash: input.Output.TransactionHash,
			}
			delete(bc.utxo[input.Address], utxoKey)
		}
	}
	if len(bc.blocks) > 10 {
		// Store in memory only last 10 blocks
		bc.blocks = bc.blocks[len(bc.blocks)-10:]
	}
	SaveToFile(b)
	// TODO Broadcast to network
	return true
}

func (bc *Blockchain) SendTransaction(tx *transaction.Transaction) bool {
	select {
	case bc.transactionsChan <- tx:
		return true
	default:
		return false
	}
}

func (bc *Blockchain) StartMiningPool(minerAddress string) {
	bc.wg.Add(1)
	bc.miningBlock = bc.NextBlock(minerAddress)

	go func() {
		defer bc.wg.Done()

		var pool []*transaction.Transaction
		ticker := time.NewTicker(time.Second * 10)
		defer ticker.Stop()

		for {
			select {
			case <-bc.ctx.Done():
				fmt.Printf("Stop blockchain")
			case tx, ok := <-bc.transactionsChan:
				if !ok {
					continue
				}
				// TODO Broadcast to network
				pool = append(pool, tx)
			// TODO Here some problem, if we start mining, we cannot added new TX in pool because of blocking!
			// TODO Separate mining from POOL
			case <-ticker.C:
				for i := range pool {

					inputs, output, ok := bc.utxo.Withdraw(pool[i].Inputs[0].Address, pool[i].OutputSum())
					if !ok {
						continue
					}
					pool[i].Inputs = inputs
					if output != (transaction.Output{}) {
						pool[i].Outputs = append(pool[i].Outputs, output)
					}
					pool[i].Seal(time.Now())
					if ok := bc.miningBlock.AddTransaction(pool[i]); !ok {
						// TODO Need to return UTXO >.< (Maybe?)
						fmt.Println("Transaction not added to block!")
					}
				}
				start := time.Now()
				bc.miningBlock.Mining()
				elapsed := time.Since(start)
				fmt.Printf("Block№ %v(%v). Mining time: %v\n", bc.miningBlock.Number, bc.miningBlock.Hash, elapsed)
				bc.AddBlock(bc.miningBlock)
				bc.miningBlock = bc.NextBlock(minerAddress)
				pool = []*transaction.Transaction{}
			}
		}
	}()
}

func StateFromFiles(bc *Blockchain) {
	if err := os.MkdirAll(blockChainDir, 0777); err != nil && !os.IsExist(err) {
		log.Fatal(err)
	}
	files, err := ioutil.ReadDir(blockChainDir)
	if err != nil {
		log.Fatal(err)
	}
	fileNames := make([]string, len(files))
	for i := range files {
		fileNames[i] = files[i].Name()
	}
	sort.Slice(fileNames, func(i, j int) bool {
		return getNumber(fileNames[i]) < getNumber(fileNames[j])
	})
	for i := range fileNames {
		pathToFile := path.Join(blockChainDir, fileNames[i])
		file, err := os.Open(pathToFile)
		if err != nil {
			log.Fatal(err)
		}

		b := &block.Block{}
		if err := json.NewDecoder(file).Decode(b); err != nil {
			log.Fatal(err)
		}
		file.Close()
		if ok := bc.AddBlock(b); !ok {
			log.Fatalf("Invalid block from file %v! Abort blockchain \n", fileNames[i])
		}
	}
}

var re = regexp.MustCompile("[0-9]+")

func getNumber(fileName string) int {
	result := re.Find([]byte(fileName))
	number, _ := strconv.Atoi(string(result))
	return number
}

func SaveToFile(b *block.Block) {
	blockFileName := fmt.Sprintf("block_%v.json", b.Number)
	pathToFile := path.Join(blockChainDir, blockFileName)
	file, err := os.Create(pathToFile)
	if err != nil {
		log.Fatal(err)
	}
	if err := json.NewEncoder(file).Encode(b); err != nil && err != io.EOF {
		log.Fatal(err)
	}
}
