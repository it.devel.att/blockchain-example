package proof_of_work

import (
	"crypto/sha256"
	"encoding/hex"
	"strconv"
	"strings"
)

const startDifficult = 4

// TODO Better logic
func RequiredComplexity(numberOfBLock int) int {
	additionalDifficult := numberOfBLock / 10
	return startDifficult + additionalDifficult
}

func IsSatisfyComplexity(numberOfBlock int, hash string) bool {
	return strings.HasPrefix(hash, strings.Repeat("0", RequiredComplexity(numberOfBlock)))
}

func GenerateHash(base string, nonce uint64) string {
	hash := sha256.Sum256([]byte(base + strconv.Itoa(int(nonce))))
	return hex.EncodeToString(hash[:])
}
