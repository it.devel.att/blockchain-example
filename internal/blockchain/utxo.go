package blockchain

import (
	"blockchain-example/internal/blockchain/transaction"
)

// key is address
type UTXO map[string]map[UTXOKey][]UTXOData

type UTXOKey struct {
	BlockNumber     int
	BlockHash       string
	TransactionHash string
}

type UTXOData struct {
	Amount uint64
}

func (u UTXO) Withdraw(address string, coins uint64) (transaction.Inputs, transaction.Output, bool) {
	if _, ok := u[address]; !ok {
		return transaction.Inputs{}, transaction.Output{}, false
	}

	var usedUtxo []UTXOKey
	var utxoCoins uint64

	inputs := transaction.Inputs{}
	for key, value := range u[address] {
		usedUtxo = append(usedUtxo, key)
		for i := range value {
			inputs = append(inputs, transaction.Input{
				Amount: value[i].Amount,
				Output: &transaction.PreviousOutput{
					BlockNumber:     key.BlockNumber,
					BlockHash:       key.BlockHash,
					TransactionHash: key.TransactionHash,
				},
				Address: address,
			})
			utxoCoins += value[i].Amount
		}
		if utxoCoins >= coins {
			break
		}
	}
	if utxoCoins < coins {
		return transaction.Inputs{}, transaction.Output{}, false
	}
	for i := range usedUtxo {
		delete(u[address], usedUtxo[i])
	}

	remains := utxoCoins - coins
	var outputForRemains transaction.Output
	if remains != 0 {
		outputForRemains = transaction.Output{Address: address, Amount: remains}
	}
	return inputs, outputForRemains, true
}
