package merkle_tree

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_GenerateRootHash(t *testing.T) {
	t.Run("Even number of transactions which can be fully divided by two", func(t *testing.T) {
		testTransactions := []string{"1", "2", "3", "4", "5", "6", "7", "8"}
		expectedMerkleRoot := leafToHash(Leaf{
			leafToHash(Leaf{leafToHash(Leaf{"1", "2"}), leafToHash(Leaf{"3", "4"})}),
			leafToHash(Leaf{leafToHash(Leaf{"5", "6"}), leafToHash(Leaf{"7", "8"})}),
		})
		expectedMerkleTree := [][]string{
			testTransactions,
			{
				leafToHash(Leaf{"1", "2"}),
				leafToHash(Leaf{"3", "4"}),
				leafToHash(Leaf{"5", "6"}),
				leafToHash(Leaf{"7", "8"}),
			},
			{
				leafToHash(Leaf{leafToHash(Leaf{"1", "2"}), leafToHash(Leaf{"3", "4"})}),
				leafToHash(Leaf{leafToHash(Leaf{"5", "6"}), leafToHash(Leaf{"7", "8"})}),
			},
		}
		merkleRoot, merkleTree := GenerateRootHash(testTransactions)

		assert.Equal(t, expectedMerkleRoot, merkleRoot)
		assert.Equal(t, expectedMerkleTree, merkleTree)
	})
	t.Run("Odd number of transactions", func(t *testing.T) {
		testTransactions := []string{"1", "2", "3", "4", "5", "6", "7", "8", "9"}
		expectedMerkleRoot := leafToHash(Leaf{
			leafToHash(Leaf{
				leafToHash(Leaf{leafToHash(Leaf{"1", "2"}), leafToHash(Leaf{"3", "4"})}),
				leafToHash(Leaf{leafToHash(Leaf{"5", "6"}), leafToHash(Leaf{"7", "8"})}),
			}),
			leafToHash(Leaf{
				leafToHash(Leaf{leafToHash(Leaf{"9", "9"}), leafToHash(Leaf{"9", "9"})}),
				leafToHash(Leaf{leafToHash(Leaf{"9", "9"}), leafToHash(Leaf{"9", "9"})}),
			}),
		})
		expectedMerkleTree := [][]string{
			testTransactions,
			{
				leafToHash(Leaf{"1", "2"}),
				leafToHash(Leaf{"3", "4"}),
				leafToHash(Leaf{"5", "6"}),
				leafToHash(Leaf{"7", "8"}),
				leafToHash(Leaf{"9", "9"}),
			},
			{
				leafToHash(Leaf{leafToHash(Leaf{"1", "2"}), leafToHash(Leaf{"3", "4"})}),
				leafToHash(Leaf{leafToHash(Leaf{"5", "6"}), leafToHash(Leaf{"7", "8"})}),
				leafToHash(Leaf{leafToHash(Leaf{"9", "9"}), leafToHash(Leaf{"9", "9"})}),
			},
			{
				leafToHash(Leaf{
					leafToHash(Leaf{leafToHash(Leaf{"1", "2"}), leafToHash(Leaf{"3", "4"})}),
					leafToHash(Leaf{leafToHash(Leaf{"5", "6"}), leafToHash(Leaf{"7", "8"})}),
				}),
				leafToHash(Leaf{
					leafToHash(Leaf{leafToHash(Leaf{"9", "9"}), leafToHash(Leaf{"9", "9"})}),
					leafToHash(Leaf{leafToHash(Leaf{"9", "9"}), leafToHash(Leaf{"9", "9"})}),
				}),
			},
		}
		merkleRoot, merkleTree := GenerateRootHash(testTransactions)

		assert.Equal(t, expectedMerkleRoot, merkleRoot)
		assert.Equal(t, expectedMerkleTree, merkleTree)
	})
	t.Run("Even number of transactions which not fully divided by two", func(t *testing.T) {
		testTransactions := []string{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}
		expectedMerkleRoot := leafToHash(Leaf{
			leafToHash(Leaf{
				leafToHash(Leaf{leafToHash(Leaf{"1", "2"}), leafToHash(Leaf{"3", "4"})}),
				leafToHash(Leaf{leafToHash(Leaf{"5", "6"}), leafToHash(Leaf{"7", "8"})}),
			}),
			leafToHash(Leaf{
				leafToHash(Leaf{leafToHash(Leaf{"9", "10"}), leafToHash(Leaf{"9", "10"})}),
				leafToHash(Leaf{leafToHash(Leaf{"9", "10"}), leafToHash(Leaf{"9", "10"})}),
			}),
		})
		expectedMerkleTree := [][]string{
			testTransactions,
			{
				leafToHash(Leaf{"1", "2"}),
				leafToHash(Leaf{"3", "4"}),
				leafToHash(Leaf{"5", "6"}),
				leafToHash(Leaf{"7", "8"}),
				leafToHash(Leaf{"9", "10"}),
			},
			{
				leafToHash(Leaf{leafToHash(Leaf{"1", "2"}), leafToHash(Leaf{"3", "4"})}),
				leafToHash(Leaf{leafToHash(Leaf{"5", "6"}), leafToHash(Leaf{"7", "8"})}),
				leafToHash(Leaf{leafToHash(Leaf{"9", "10"}), leafToHash(Leaf{"9", "10"})}),
			},
			{
				leafToHash(Leaf{
					leafToHash(Leaf{leafToHash(Leaf{"1", "2"}), leafToHash(Leaf{"3", "4"})}),
					leafToHash(Leaf{leafToHash(Leaf{"5", "6"}), leafToHash(Leaf{"7", "8"})}),
				}),
				leafToHash(Leaf{
					leafToHash(Leaf{leafToHash(Leaf{"9", "10"}), leafToHash(Leaf{"9", "10"})}),
					leafToHash(Leaf{leafToHash(Leaf{"9", "10"}), leafToHash(Leaf{"9", "10"})}),
				}),
			},
		}
		merkleRoot, merkleTree := GenerateRootHash(testTransactions)

		assert.Equal(t, expectedMerkleRoot, merkleRoot)
		assert.Equal(t, expectedMerkleTree, merkleTree)
	})
}

func Test_GenerateAuthPath(t *testing.T) {
	testTransactions := []string{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}
	merkleTree := [][]string{
		testTransactions,
		{
			leafToHash(Leaf{"1", "2"}),
			leafToHash(Leaf{"3", "4"}),
			leafToHash(Leaf{"5", "6"}),
			leafToHash(Leaf{"7", "8"}),
			leafToHash(Leaf{"9", "10"}),
		},
		{
			leafToHash(Leaf{leafToHash(Leaf{"1", "2"}), leafToHash(Leaf{"3", "4"})}),
			leafToHash(Leaf{leafToHash(Leaf{"5", "6"}), leafToHash(Leaf{"7", "8"})}),
			leafToHash(Leaf{leafToHash(Leaf{"9", "10"}), leafToHash(Leaf{"9", "10"})}),
		},
		{
			leafToHash(Leaf{
				leafToHash(Leaf{leafToHash(Leaf{"1", "2"}), leafToHash(Leaf{"3", "4"})}),
				leafToHash(Leaf{leafToHash(Leaf{"5", "6"}), leafToHash(Leaf{"7", "8"})}),
			}),
			leafToHash(Leaf{
				leafToHash(Leaf{leafToHash(Leaf{"9", "10"}), leafToHash(Leaf{"9", "10"})}),
				leafToHash(Leaf{leafToHash(Leaf{"9", "10"}), leafToHash(Leaf{"9", "10"})}),
			}),
		},
	}
	expectedMerklePath := []AuthLeaf{
		{ID: 4, Hash: "5"},
		{ID: 3, Hash: leafToHash(Leaf{"7", "8"})},
		{ID: 0, Hash: leafToHash(Leaf{leafToHash(Leaf{"1", "2"}), leafToHash(Leaf{"3", "4"})})},
		{ID: 1, Hash: leafToHash(Leaf{
			leafToHash(Leaf{leafToHash(Leaf{"9", "10"}), leafToHash(Leaf{"9", "10"})}),
			leafToHash(Leaf{leafToHash(Leaf{"9", "10"}), leafToHash(Leaf{"9", "10"})}),
		})},
	}
	merklePath := GenerateMerklePath("6", merkleTree)
	assert.Equal(t, expectedMerklePath, merklePath)
}

func Test_GenerateFromAuthPath(t *testing.T) {
	hashToFind := "6"
	testTransactions := []string{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}
	expectedMerkleRoot, _ := GenerateRootHash(testTransactions)
	authPath := []AuthLeaf{
		{ID: 4, Hash: "5"},
		{ID: 3, Hash: leafToHash(Leaf{"7", "8"})},
		{ID: 0, Hash: leafToHash(Leaf{leafToHash(Leaf{"1", "2"}), leafToHash(Leaf{"3", "4"})})},
		{ID: 1, Hash: leafToHash(Leaf{
			leafToHash(Leaf{leafToHash(Leaf{"9", "10"}), leafToHash(Leaf{"9", "10"})}),
			leafToHash(Leaf{leafToHash(Leaf{"9", "10"}), leafToHash(Leaf{"9", "10"})}),
		})},
	}
	authMerkleRoot := GenerateFromMerklePath(hashToFind, authPath)
	assert.Equal(t, expectedMerkleRoot, authMerkleRoot)
}
