package main

import (
	"bytes"
	"crypto/sha256"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"blockchain-example/internal/wallet/accounts"
)

// For local testing purpose it's better to have same wallets as miner has
const walletsFileName = accounts.WalletsFile

var wallets = make(accounts.Accounts)

type TxData struct {
	From      string `json:"from"`
	To        string `json:"to"`
	Amount    uint64 `json:"amount"`
	Signature string `json:"signature"`
}

func (tx *TxData) Hash() []byte {
	hash := sha256.Sum256([]byte(tx.From + tx.To + strconv.Itoa(int(tx.Amount))))
	return hash[:]
}

func main() {
	generateAccount := flag.Bool("generate-address", false, "Generate private and public key")
	printAddresses := flag.Bool("print-addresses", false, "Printed your addresses")
	//Send transaction
	sendTransaction := flag.Bool("send", false, "Create transaction and send it to node")
	from := flag.String("from", "", "Your address from which you want to send coins")
	to := flag.String("to", "", "Address to which you want to send coins")
	amount := flag.Uint64("amount", 0, "Amount of coins you want to send")

	flag.Parse()
	accounts.LoadWallets(&wallets, walletsFileName)
	defer func() {
		accounts.SaveWallets(&wallets, walletsFileName)
	}()

	if *generateAccount {
		account := accounts.GenerateAccount()
		wallets.SaveAccount(account)
		fmt.Println("New address: ", account.Address)
		return
	}
	if *printAddresses {
		wallets.PrintAddresses()
		return
	}
	if *sendTransaction {
		fromAddress := *from
		toAddress := *to
		amountOfCoins := *amount
		account, ok := wallets[fromAddress]
		if !ok {
			fmt.Printf("You don't have wallet with address: %v\n", fromAddress)
			return
		}
		txData := TxData{
			From:   account.PublicHexKey,
			To:     toAddress,
			Amount: amountOfCoins,
		}
		txData.Signature = account.Sign(txData.Hash())
		fmt.Printf("Send %v coins from %v to %v\n", amountOfCoins, fromAddress, toAddress)
		data, err := json.Marshal(txData)
		if err != nil {
			log.Fatal(err)
		}
		// TODO wrap into some client!
		resp, err := http.Post("http://localhost:9898/api/v1/transaction", "application/json", bytes.NewReader(data))
		if err != nil {
			log.Fatal(err)
		}
		response := map[string]interface{}{}
		json.NewDecoder(resp.Body).Decode(&response)
		fmt.Printf("%v\n", response)
	}
}
