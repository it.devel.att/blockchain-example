package main

import (
	"context"
	"crypto/ecdsa"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net"
	"net/http"
	"strconv"

	"blockchain-example/internal/blockchain"
	"blockchain-example/internal/blockchain/transaction"
	"blockchain-example/internal/wallet/accounts"
)

type TxData struct {
	From      string
	To        string
	Amount    uint64
	Signature string
}

func (data *TxData) GenHash() []byte {
	hash := sha256.Sum256([]byte(data.From + data.To + strconv.Itoa(int(data.Amount))))
	return hash[:]
}

func (data *TxData) SignatureValid() bool {
	pubKey := accounts.UnmarshallHEXPublicKey(data.From)
	signBytes, err := hex.DecodeString(data.Signature)
	if err != nil {
		return false
	}
	return ecdsa.VerifyASN1(pubKey, data.GenHash(), signBytes)
}

// For local testing purpose it's better to have same wallets as client has
const minerWallet = accounts.WalletsFile

var wallets = make(accounts.Accounts)

func main() {
	walletAddress := flag.String("wallet", "", "Wallet which use for mining")
	flag.Parse()

	var minerAccount accounts.Account

	accounts.LoadWallets(&wallets, minerWallet)
	if *walletAddress != "" {
		wallet, ok := wallets[*walletAddress]
		if !ok {
			fmt.Println("You don't have account with address: ", *walletAddress)
			return
		}
		minerAccount = wallet
	} else {
		minerAccount = accounts.GenerateAccount()
		wallets.SaveAccount(minerAccount)
	}
	fmt.Println("All miner addresses")
	wallets.PrintAddresses()
	fmt.Println("Current used MinerAddress: ", minerAccount.Address)
	accounts.SaveWallets(&wallets, minerWallet)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	//wg := sync.WaitGroup{}

	zupperChain := blockchain.LoadBlockchain(ctx)
	zupperChain.StartMiningPool(minerAccount.Address)

	handler := mux.NewRouter()
	// TODO Move into some server/http package!
	handler.HandleFunc("/api/v1/transaction", func(w http.ResponseWriter, r *http.Request) {
		txData := &TxData{}
		if err := json.NewDecoder(r.Body).Decode(txData); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(map[string]string{"error": "invalid body"})
			return
		}
		if !txData.SignatureValid() {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(map[string]string{"error": "transaction signature invalid"})
			return
		}
		tx := transaction.NewTransaction()
		address := accounts.GenerateShortAddress(accounts.UnmarshallHEXPublicKey(txData.From))
		tx.Inputs = transaction.Inputs{{Amount: txData.Amount, Address: address}}
		tx.Outputs = transaction.Outputs{{Amount: txData.Amount, Address: txData.To}}
		zupperChain.SendTransaction(tx)
		w.WriteHeader(200)
		json.NewEncoder(w).Encode(tx)
	}).Methods(http.MethodPost)

	if err := http.ListenAndServe(":9898", handler); err != nil && err != net.ErrClosed {
		log.Fatal(err)
	}
}
