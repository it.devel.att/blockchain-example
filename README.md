
Run blockchain network with miner
```shell script
go run cmd/blockchain/main.go
```

Generate few addresses
```shell script
go run cmd/wallet/main.go -generate-address
go run cmd/wallet/main.go -generate-address
go run cmd/wallet/main.go -generate-address
```
```shell script
address:  13df344b4e84a48b4f84827723d508068218e9ea
address:  2474f373e6274bf75a538a0bce5140aebf4072b9
address:  9763010b303dbfd05b4db4a8cefd6016b73b568a
```

For send coins from one address to another
```shell script
go run cmd/wallet/main.go -send -from 13df344b4e84a48b4f84827723d508068218e9ea -to 2474f373e6274bf75a538a0bce5140aebf4072b9 -amount 50
```